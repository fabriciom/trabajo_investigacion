package pds_ufro.jersey_prueba;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

import domain.Enfermedad;
import domain.Negocio;
@Path("searchdiseasews/{busqueda}")
public class SearchDiseaseService {

	/**
	 * 
	 * @param busqueda
	 *            de la enfermedad
	 * @return Lista de enfermedades
	 */
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public static List<Enfermedad> searchDisease(@PathParam("busqueda") String busqueda) {
		return Negocio.searchDisease(busqueda);
	}

}
