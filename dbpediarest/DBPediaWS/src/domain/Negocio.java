package domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.RDFNode;

public class Negocio {

	/**
	 * 
	 * @param busqueda
	 *            parametro para buscar la enfermedad
	 * @return Lista de Enfermedades
	 */
	public static List<Enfermedad> searchDisease(String busqueda) {

		List<Enfermedad> enfermedades = new ArrayList<Enfermedad>();
		// Prefijos requeridos
		String prefijos = "PREFIX owl: <http://www.w3.org/2002/07/owl#>"
				+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
				+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
				+ "PREFIX foaf: <http://xmlns.com/foaf/0.1/>"
				+ "PREFIX dc: <http://purl.org/dc/elements/1.1/>"
				+ "PREFIX : <http://dbpedia.org/resource/>"
				+ "PREFIX dbpedia2: <http://dbpedia.org/property/>"
				+ "PREFIX dbpedia: <http://dbpedia.org/>"
				+ "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>"
				+ "PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>";

		// Consulta en sparql
		String queryString = prefijos
				+ "SELECT ?fuente ?nombre ?descripcion WHERE{"
				+ "?fuente rdf:type <http://dbpedia.org/ontology/Disease>;"
				+ "rdfs:label ?nombre;"
				+ "dbpedia-owl:abstract ?descripcion."
				+ "FILTER (lang(?nombre) = \"es\" &&  lang(?descripcion) = \"es\" && regex(?nombre, \"^"+busqueda+"\",\"i\"))}";
		Query query = QueryFactory.create(queryString);

		// Ejecuta consulta en el endpoint dbpedia.org/sparql
		QueryExecution qexec = QueryExecutionFactory.sparqlService(
				"http://dbpedia.org/sparql", query);

		try {
			// Entrega los resultados de la consulta
			ResultSet rs = qexec.execSelect();

			while (rs.hasNext()) {
				// toma el siguiente resultado
				QuerySolution qs = rs.nextSolution();
				// Obtiene el valor de la variable indicada, en este caso
				// descripción por poner un ejemplo
				Literal nombre = qs.getLiteral("nombre");
				Literal descripcion = qs.getLiteral("descripcion");
				RDFNode fuente = qs.get("fuente");
				// Imprime la descripción. si no se usa getString() entonces
				// agrega @es al final o lo que sea que indique el idioma
				enfermedades.add(new Enfermedad(nombre.getString(), descripcion
						.getString(), fuente.toString()));
				//System.out.println(qs.get("fuente"));

			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("No existen datos para mostrar");
			return null;
		} finally {
			qexec.close();
		}
		return enfermedades;

	}
	
public static void main(String[] args){
	System.out.println(searchDisease("Cáncer"));
}
}
