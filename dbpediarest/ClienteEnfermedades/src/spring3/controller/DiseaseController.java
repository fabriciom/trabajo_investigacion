package spring3.controller;


import java.rmi.RemoteException;

import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import domain.Enfermedad;
import webservice.SearchDiseaseServiceStub;
import webservice.SearchDiseaseServiceStub.SearchDisease;

@Controller
public class DiseaseController {

	@RequestMapping(value="/searchdisease.html")
	public ModelAndView desplegarForm(){
		return new ModelAndView("searchdisease");
	}
	@RequestMapping("/procesar.html")
	public ModelAndView procesar(@RequestParam("busqueda") String busqueda) throws RemoteException{
		if(busqueda==""){
			return new ModelAndView("searchdisease");
		}
		ClientConfig config = new DefaultClientConfig();
		Client cliente = new Client().create(config);
		
		WebResource webResource = cliente.resource("http://localhost:8080/JerseyPrueba2/webapi/searchdiseasews/"+busqueda);
		ClientResponse response = webResource.type(MediaType.APPLICATION_XML).get(ClientResponse.class);
		Enfermedad[] c = response.getEntity(Enfermedad[].class);
		System.out.println(busqueda);
		return new ModelAndView("result","Result",c);
	}
}
