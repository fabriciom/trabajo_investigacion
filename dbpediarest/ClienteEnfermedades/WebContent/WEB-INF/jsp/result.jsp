<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Ejemplo de Resultado en Tabla</title>
    <meta charset="utf-8" />
</head>

	<form:form method="get" action="procesar.html">
	<h1>Buscar Enfermedad</h1>
		<input type="text" name="busqueda"/>
		<input type="submit" value="Buscar" />
	</form:form>
<body>
	
    <c:forEach items="${Result}" var="enfermedad" varStatus="status">
            <h2>${enfermedad.nombre}</h2>
            <p><h3>Descripcion</h3>${enfermedad.descripcion}
            <h3>Fuente: </h3><a href="${enfermedad.fuente}">${enfermedad.fuente}</a>
            </p>
            
    </c:forEach>
<br/>
<input type="button" value="Back" onclick="javascript:history.back()"/>
</body>
<style>
body{
	color: black;
	background: aqua;
	font-family: "Helvetica", Arial;
	font-size: 16px;
	margin: 1em 300px;
	text-align: left;
}
form
{

	text-align: center;
	text-shadow: 5px 5px 5px rgba(0,0,0,0.5);
	margin: auto auto;
	padding: 1em;
	width: 100%;
}
form input
{
	display: block;
	font-size: 20px;
	margin: 1em auto;
	padding: 0.5em;
	width: 70%;
}
	</style>
</html>