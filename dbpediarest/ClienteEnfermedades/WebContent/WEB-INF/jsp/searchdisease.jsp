<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Buscar</title>
</head>
<body>
	<h1>Buscar Enfermedad</h1>
	<form:form method="post" action="procesar.html">
		<input type="text" name="busqueda"/>
		<input type="submit" value="Buscar" />
	</form:form>
</body>
<style>
body{
	color: black;
	background: aqua;
	font-family: "Helvetica", Arial;
	font-size: 48px;
	text-align: center;
}
form
{
	
	text-shadow: 5px 5px 5px rgba(0,0,0,0.5);
	margin: 1em auto;
	padding: 1em;
	width: 70%;
}
form input
{
	display: block;
	font-size: 40px;
	margin: 1em auto;
	padding: 0.5em;
	width: 70%;
}
	</style>
</html>