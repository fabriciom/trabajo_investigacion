package webservice;

import java.util.List;

import com.google.gson.Gson;

import domain.Enfermedad;
import domain.Negocio;

public class SearchDiseaseService {

	/**
	 * 
	 * @param busqueda
	 *            de la enfermedad
	 * @return Lista de enfermedades
	 */
	public static List<Enfermedad> searchDisease(String busqueda) {
		return Negocio.searchDisease(busqueda);
	}

	public static String searchDiseaseAndroid(String busqueda) {
		  
		
		return new Gson().toJson(Negocio.searchDisease(busqueda));
	}

	public static void main(String[] args){
		System.out.println(Negocio.searchDisease("Cáncer").get(0));
	}
	
}
