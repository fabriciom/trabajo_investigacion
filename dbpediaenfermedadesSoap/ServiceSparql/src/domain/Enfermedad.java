package domain;

public class Enfermedad {
	
	
	private String nombre;
	private String descripcion;
	private String fuente;

	
	public Enfermedad() {
		super();
	}

	/**
	 * 
	 * @param nombre de la enfermedad
	 * @param descripcion de la enfermedad
	 * @param fuente de la enfermedad
	 */
	public Enfermedad(String nombre, String descripcion, String fuente) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fuente = fuente;
	}

	/**
	 * Devuelve el nombre
	 * @return String con el nombre de la enfermedad
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * 
	 * @param nombre de la enfermeda
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * 
	 * @return String con la descripción de la enfermedad
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * 
	 * @param descripcion de la enfermedad
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * 
	 * @return String con el link de referencia
	 */
	public String getFuente() {
		return fuente;
	}

	/**
	 * 
	 * @param fuente con el link de referencia
	 */
	public void setFuente(String fuente) {
		this.fuente = fuente;
	}

	@Override
	public String toString() {
		return "Enfermedad [nombre=" + nombre + ", descripcion=" + descripcion
				+ ", fuente=" + fuente + "]";
	}
	
	

}
