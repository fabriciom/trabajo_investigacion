<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
    <title>Resultados encontrados</title>
</head>
<body>

<h4>${resultadosEncontrados}</h4>

<table width="50%">
 
    <c:forEach items="${Result}" var="enfermedad" varStatus="status">
        <tr><td><h2>${enfermedad.nombre}</h2></td></tr>
         <tr><td><strong>Descripci&oacute;n : </strong></td></tr>
            <tr><td>${enfermedad.descripcion}</td></tr>
            <tr><td><strong>Fuente: </strong></td></tr>
            <tr><td><a href="${enfermedad.fuente}">${enfermedad.fuente}</a></td></tr>
    </c:forEach>
</table>  
<br/>
<input type="button" value="Back" onclick="javascript:history.back()"/>
</body>
</html>