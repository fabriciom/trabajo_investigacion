
/**
 * SearchDiseaseServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package webservice;

    /**
     *  SearchDiseaseServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class SearchDiseaseServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public SearchDiseaseServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public SearchDiseaseServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
               // No methods generated for meps other than in-out
                
           /**
            * auto generated Axis2 call back method for searchDisease method
            * override this method for handling normal response from searchDisease operation
            */
           public void receiveResultsearchDisease(
                    webservice.SearchDiseaseServiceStub.SearchDiseaseResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from searchDisease operation
           */
            public void receiveErrorsearchDisease(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for searchDiseaseAndroid method
            * override this method for handling normal response from searchDiseaseAndroid operation
            */
           public void receiveResultsearchDiseaseAndroid(
                    webservice.SearchDiseaseServiceStub.SearchDiseaseAndroidResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from searchDiseaseAndroid operation
           */
            public void receiveErrorsearchDiseaseAndroid(java.lang.Exception e) {
            }
                


    }
    