package spring3.controller;


import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import spring3.form.DiseaseSearchForm;
import webservice.SearchDiseaseServiceStub;
import webservice.SearchDiseaseServiceStub.Enfermedad;
import webservice.SearchDiseaseServiceStub.SearchDisease;
import webservice.SearchDiseaseServiceStub.SearchDiseaseResponse;

@Controller
public class DiseaseController {

	@RequestMapping(value="/searchdisease.html")
	public ModelAndView desplegarForm(){
		return new ModelAndView("searchdisease");
	}
	@RequestMapping("/procesar.html")
	public ModelAndView procesar(@ModelAttribute("diseaseSearchForm") @Valid  DiseaseSearchForm diseaseSearchForm, BindingResult result) throws RemoteException{
        //BindingResult result, 
   	 if(result.hasErrors()) {
            return new ModelAndView("searchdisease");
        } 
		Enfermedad[] enfermedades;
		String resultadosEncontrados;
		SearchDiseaseServiceStub oStub = new SearchDiseaseServiceStub();
		SearchDisease searchDisease = new SearchDisease();
		searchDisease.setBusqueda(diseaseSearchForm.getBusqueda());
		
		SearchDiseaseResponse objResponse = oStub.searchDisease(searchDisease);
		enfermedades=objResponse.get_return();
		if(enfermedades==null)
		{
			resultadosEncontrados="No se han encontrado resultados para " + diseaseSearchForm.getBusqueda(); 
		}
		else{
			resultadosEncontrados="Se han encontrado " + enfermedades.length + " resultados para " + diseaseSearchForm.getBusqueda(); 
		}
		
		ModelAndView modelAndView = new ModelAndView("result","Result",enfermedades);
		modelAndView.addObject("resultadosEncontrados", resultadosEncontrados);
		
		return modelAndView; 
		
	}
}
