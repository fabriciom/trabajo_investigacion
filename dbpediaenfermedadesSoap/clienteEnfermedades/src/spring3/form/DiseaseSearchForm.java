package spring3.form;

import org.hibernate.validator.constraints.NotEmpty;


public class DiseaseSearchForm {

	@NotEmpty
	private String busqueda;

	public String getBusqueda() {
		return busqueda;
	}

	public void setBusqueda(String busqueda) {
		this.busqueda = busqueda;
	}
		
}
